<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <h1>Person</h1>
  <p><?php var_dump($person);
      ?></p>

  <p><?= $person->printFullName() ?></p>


  <h1>Developer</h1>
  <p><?php var_dump($developer);
      ?></p>

  <p><?= $developer->printFullName() ?></p>

  <h1>Engineer</h1>

  <p><?php var_dump($engineer);
      ?></p>

  <p><?= $engineer->printFullName() ?></p>
</body>

</html>